import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http'; // for rest-api
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ControlComponent } from './control/control.component';
import { DataComponent } from './data/data.component';
import { DiagramComponent } from './diagram/diagram.component';
import { DiagramElectricalComponent } from './diagram/diagram-electrical/diagram-electrical.component';
import { DiagramMechanicalComponent } from './diagram/diagram-mechanical/diagram-mechanical.component';
import { HttpClient} from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { RestService } from './rest.service';
import { ExchangeService } from './websocket/exchange.service';
import { WebsocketService } from './websocket/websocket.service';
import { DataService} from './data/data.service';
import { ControlDataService } from './control/control.service';
import { WebsocketEventsService } from './websocket/websocket-events.service';
import { IntroductionComponent } from './introduction/introduction.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule, //for rest-api
    AppRoutingModule,
    HttpClientModule // for fix no provider
  ],
  declarations: [
    AppComponent,
    ControlComponent,
    DataComponent,
    DiagramComponent,
    DiagramElectricalComponent,
    DiagramMechanicalComponent,
    IntroductionComponent
  ],
  providers: [
    RestService,
    HttpClient,
    ExchangeService,
    WebsocketService,
    DataService,
    ControlDataService,
    WebsocketEventsService
  ],
  bootstrap: [AppComponent]
})

export class AppModule {}
