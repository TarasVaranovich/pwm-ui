import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ControlComponent } from './control/control.component';
import { DataComponent } from './data/data.component';
import { DiagramComponent } from './diagram/diagram.component';
import { DiagramElectricalComponent } from './diagram/diagram-electrical/diagram-electrical.component';
import { DiagramMechanicalComponent } from './diagram/diagram-mechanical/diagram-mechanical.component';
import { IntroductionComponent } from './introduction/introduction.component';

const routes: Routes = [
  { path: 'introduction',     component: IntroductionComponent },
  { path: 'control',     component: ControlComponent },
  { path: 'data',     component: DataComponent },
  { path: 'diagram',     component: DiagramComponent,
  children: [
        { path: 'diagram-electrical', component: DiagramElectricalComponent },
        { path: 'diagram-mechanical', component: DiagramMechanicalComponent }
      ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
