import { HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class RestService {

    private actionStartUrl: string;
    private actionStopUrl: string;
    private actionRestartUrl: string;
    private actionExitUrl: string;

    constructor(private http: HttpClient) {
        this.actionStartUrl = '/pwm/index.htm/start.htm';
        this.actionStopUrl = '/pwm/index.htm/stop.htm';
        this.actionRestartUrl ='/pwm/index.htm/restart.htm';
        this.actionExitUrl = '/pwm/index.htm/exit.htm'
    }

    public startProc<T>(controlVoltage: number,
                        controlMaxVoltage:number,
                        controlVoltageDeflection: number,
                        controlCalcPrecision: number,
                        controlPolesPairCount: number,
                        controlNominalFrequence: number,
                        controlRevolutions: number,
                        controlRevolutionsChange: number,
                        controlDeflectionChange: number): Observable<T> {

        const startParam = JSON.stringify({ controlVoltage: controlVoltage,
                                            controlMaxVoltage: controlMaxVoltage,
                                            controlVoltageDeflection: controlVoltageDeflection,
                                            controlCalcPrecision: controlCalcPrecision,
                                            controlPolesPairCount: controlPolesPairCount,
                                            controlNominalFrequence: controlNominalFrequence,
                                            controlRevolutions: controlRevolutions,
                                            controlRevolutionsChange: controlRevolutionsChange,
                                            controlDeflectionChange: controlDeflectionChange});


        //return this.http.post<T>(this.actionStartUrl, startParam).retry(3);
        return this.http.post('http://localhost:8080/pwm/index.htm/start.htm', startParam);
    }

    public stopProc<T>(): Observable<T> {
        //return this.http.get<T>(this.actionStopUrl);
        return this.http.get('http://localhost:8080/pwm/index.htm/stop.htm');
    }

    public restartProc<T>(): Observable<T> {
        //return this.http.get<T>(this.actionRestartUrl);
        return this.http.get('http://localhost:8080/pwm/index.htm/restart.htm');
    }

    public exitProc<T>(): Observable<T> {
        //return this.http.get<T>(this.actionRestartUrl);
        return this.http.get('http://localhost:8080/pwm/index.htm/exit.htm');
    }
}
