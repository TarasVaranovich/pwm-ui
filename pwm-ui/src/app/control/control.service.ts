import { Injectable } from '@angular/core';

@Injectable()
export class ControlDataService {
  private directVoltage: number = 700;
  private maxVoltage: number = 380;
  private voltageDeflection: number = 5; //--
  private calculationPrecision: number = 10;
  private polesPairCount: number = 2;
  private nominalFrequence: number = 50;
  private revolutions: number = 3000;
  private revolutionsChange: number = 100;//--
  private deflectionChange: number = 1;//-

  public controlDataPart() {

  }

  public setRevolutions(revolutions: number): void{
    this.revolutions = revolutions;
  }

  public setNominalFrequence(nominalFrequence: number): void{
    this.nominalFrequence = nominalFrequence;
  }

  public setDirectVoltage(directVoltage: number): void{
    this.directVoltage = directVoltage;
  }

  public setMaxVoltage(maxVoltage: number): void {
    this.maxVoltage = maxVoltage;
  }

  public setCalculationPrecision(calculationPrecision: number): void{
    this.calculationPrecision = calculationPrecision;
  }

  public setPolesPairCount(polesPairCount: number): void{
    this.polesPairCount = polesPairCount;
  }

  public setVoltageDeflection(voltageDeflection: number): void {
    this.voltageDeflection = voltageDeflection;
  }

  public setRevolutionsChange(revolutionsChange: number):void {
    this.revolutionsChange = revolutionsChange;
  }

  public setDeflectionChange(deflectionChange: number): void {
    this.deflectionChange = deflectionChange;
  }

  public getRevolutions(): number{
    return this.revolutions;
  }

  public getNominalFrequence(): number{
    return this.nominalFrequence;
  }

  public getDirectVoltage(): number{
    return this.directVoltage;
  }

  public getMaxVoltage(): number{
    return this.maxVoltage;
  }

  public getCalculationPrecision(): number{
    return this.calculationPrecision;
  }

  public getPolesPairCount(): number{
    return this.polesPairCount;
  }

  public getVoltageDeflection(): number {
    return this.voltageDeflection;
  }

  public getRevolutionsChange(): number {
    return this.revolutionsChange;
  }

  public getDeflectionChange(): number {
    return this.deflectionChange;
  }

  public getMaxTorque(): number {
    let maxTorque = (3 * Math.pow(this.maxVoltage, 2) * this.polesPairCount)/(4 * Math.PI * this.nominalFrequence *(1.09 + 1.69));
    maxTorque = maxTorque/10;
    maxTorque = Math.round(maxTorque);
    maxTorque = maxTorque * 10;
    console.log(maxTorque);
    return maxTorque;
  }

  public getMaxFrequence(): number {
    let maxFrequence = this.polesPairCount * this.revolutions/60;
    return maxFrequence;
  }

}
