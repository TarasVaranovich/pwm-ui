import 'rxjs/add/operator/toPromise';
//import 'reflect-metadata';

import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { ControlDataService } from './control.service';

/*function Console(target) {
  console.log('Our decorated class', target);
}*/

@Component({
  selector: 'pwm-control',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.css']
})

//@Console
export class ControlComponent implements OnInit {

  private controlVoltage: number;
  private controlMaxVoltage: number;
  private controlVoltageDeflection: number;
  private controlCalcPrecision: number;
  private controlPolesPairCount: number;
  private controlNominalFrequence: number;
  private controlRevolutions: number;
  private controlRevolutionsChange: number;
  private controlDeflectionChange: number;
  private startResultData: any;
  private stopResultData: any;
  private controlAlertMessage: string;


  constructor(private controlDataService: ControlDataService) {
    this.controlVoltage = 380;
    this.controlMaxVoltage = 700;
    this.controlVoltageDeflection = 5;
    this.controlCalcPrecision = 10;
    this.controlPolesPairCount = 2;
    this.controlNominalFrequence = 50;
    this.controlRevolutions  = 3000;
    this.controlRevolutionsChange = 100;
    this.controlDeflectionChange = 1;
    //this.controlAlertMessage = "ControlMessage";
    this.controlAlertMessage = null;
  }

  /*set setcontrolAlertMessage(controlAlertMessage: string) {
    this.controlAlertMessage = controlAlertMessage;
  }*/

  ngOnInit() {

  }

  confirmBtn(){
      this.sendData();
  }
  private sendData(): void {
    // send data to other components
    this.controlDataService.setRevolutions(this.controlRevolutions);
    this.controlDataService.setDirectVoltage(this.controlMaxVoltage);
    this.controlDataService.setMaxVoltage(this.controlVoltage);
    this.controlDataService.setPolesPairCount(this.controlPolesPairCount);
    this.controlDataService.setNominalFrequence(this.controlNominalFrequence);
    this.controlDataService.setCalculationPrecision(this.controlCalcPrecision);
    this.controlDataService.setDeflectionChange(this.controlDeflectionChange);
    this.controlDataService.setVoltageDeflection(this.controlVoltageDeflection);
    this.controlDataService.setRevolutionsChange(this.controlRevolutionsChange);
    //this.controlDataService.getMaxTorque();
  }
  resetInputStyle(event: KeyboardEvent): void {
    (<HTMLInputElement>event.target).classList.remove("control__input-wrong");
  }
  validateData(event: KeyboardEvent): void {
    let value:string = (<HTMLInputElement>event.target).value;
    let name: string = (<HTMLInputElement>event.target).name;

    if(this.validateNumber(value) != null){
      (<HTMLInputElement>event.target).classList.add("control__input-wrong");
      this.controlAlertMessage = this.validateNumber(value);
    } else {
      switch(name){
        case "controlMaxVoltage": {
          if(this.validateVoltage(value) != null) {
            this.controlAlertMessage = this.validateVoltage(value);
            this.noteVoltages(true);
          } else {
            this.noteVoltages(false);
          }
          break;
        }
        case "controlVoltage": {
          if(this.validateVoltage(value) != null) {
            this.controlAlertMessage = this.validateVoltage(value);
            this.noteVoltages(true);
          } else {
            this.noteVoltages(false);
          }
          break;
        }
        case ("controlVoltageDeflection"): {
          if(this.validateDeflection(value) != null) {
            this.controlAlertMessage = this.validateDeflection(value);
            (<HTMLInputElement>event.target).classList.add("control__input-wrong");
          } else {
            this.noteDeflections(false);
          }
          break;
        }
        case "controlDeflectionChange": {
          if(this.validateDeflection(value) != null) {
            this.controlAlertMessage = this.validateDeflection(value);
            //(<HTMLInputElement>event.target).classList.add("control__input-wrong");
          } else {
            this.noteDeflections(false);
          }
          break;
        }
        case "controlRevolutions": {
          if(this.validateRevolutions(value) != null) {
            this.controlAlertMessage = this.validateRevolutions(value);
            this.noteRevolutions(true);
          } else {
            this.noteRevolutions(false);
          }
          break;
        }
        case "controlRevolutionsChange": {
          if(this.validateRevolutions(value) != null) {
            this.controlAlertMessage = this.validateRevolutions(value);
            this.noteRevolutions(true);
          } else {
            this.noteRevolutions(false);
          }
          break;
        }
        default: {
          break;
        }
      }
    }

  }

  private validateNumber(value: string): string {
    //console.log(Number(value));
    if((!Number(value) && Number(value) != 0) || value==''){
      return "Not a number.";
    } else if(Number(value) <= 0){
      return "Must be greater than zero.";
    } else {
      return null;
    }
  }

  private validateVoltage(value: string): string {
    if(this.controlVoltage > this.controlMaxVoltage) {
      return "Direct voltage must be greater than maximal consumer voltage.";
    } else {
        return null;
    }
  }

  private noteVoltages(ind: boolean): void{
    var controlVoltage = document.getElementsByName("controlVoltage");
    var controlMaxVoltage = document.getElementsByName("controlMaxVoltage");
    if(ind) {
      controlVoltage[0].classList.add("control__input-wrong");
      controlMaxVoltage[0].classList.add("control__input-wrong");
    } else {
      controlVoltage[0].classList.remove("control__input-wrong");
      controlMaxVoltage[0].classList.remove("control__input-wrong");
    }
  }

  private validateDeflection(value): string {
    if(this.controlVoltageDeflection > 100) {
      return "Voltage deflection can't be more than 100%.";
    } else if(this.controlVoltageDeflection < this.controlDeflectionChange){
      this.noteDeflections(true);
      return "Deflection change delta must be less than deflection.";
    } else {
      return null;
    }
  }

  private noteDeflections(ind: boolean): void {
    var controlVoltageDeflection = document.getElementsByName("controlVoltageDeflection");
    var controlDeflectionChange = document.getElementsByName("controlDeflectionChange");
    if(ind) {
      controlVoltageDeflection[0].classList.add("control__input-wrong");
      controlDeflectionChange[0].classList.add("control__input-wrong");
    } else {
      controlVoltageDeflection[0].classList.remove("control__input-wrong");
      controlDeflectionChange[0].classList.remove("control__input-wrong");
    }
  }

  private validateRevolutions(value): string {
    if(this.controlRevolutions < this.controlRevolutionsChange) {
      return "Revolutions change delta must be less than revolutions.";
    } else {
      return null;
    }
  }

  private noteRevolutions(ind: boolean): void {
    var controlRevolutions = document.getElementsByName("controlRevolutions");
    var controlRevolutionsChange = document.getElementsByName("controlRevolutionsChange");
    if(ind) {
      controlRevolutions[0].classList.add("control__input-wrong");
      controlRevolutionsChange[0].classList.add("control__input-wrong");
    } else {
      controlRevolutions[0].classList.remove("control__input-wrong");
      controlRevolutionsChange[0].classList.remove("control__input-wrong");
    }
  }

  controlAlertBtn(){
    this.controlAlertMessage = null;
  }
}
