import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs/Rx';
import { WebsocketService } from './websocket.service';

//const CHAT_URL = 'ws://localhost:8080/project31.2/counter';
const PWM_URL = 'ws://localhost:8080/pwm/pwmdata.htm';

export interface Message {
  counter: string;
  maxMoment: string
  frequence: string
  revolutions: string;
  tableData: string
}


@Injectable()
export class ExchangeService {
  public messages: Subject<Message>;
  constructor(wsService: WebsocketService) {
    this.messages = <Subject<Message>>wsService
      .connect(PWM_URL)
      .map((response: MessageEvent): Message => {
        if(response.type == 'message'){
        let data = JSON.parse(response.data);
        return {
            counter: data.counter,
            maxMoment: data.maxMoment,
            frequence: data.frequence,
            revolutions: data.revolutions,
            tableData: data.tableData
          }
        }
      });

  }

}
