export class WebSocketEvent {
  private state: number;
  private description: string;

  public WebSocketEvent() {

  }

  public setState(state: number) {
    this.state = state;
  }

  public setDescription(description: string) {
    this.description = description;
  }

  public getState(): number {
    return this.state;
  }

  public getDescription(): string {
    return this.description;
  }
}
