import { Injectable } from '@angular/core';
import * as Rx from 'rxjs/Rx';
import { WebsocketEventsService } from './websocket-events.service';
import { WebSocketEvent } from './websocket-event';

@Injectable()
export class WebsocketService {

  private subject: Rx.Subject<MessageEvent>;

  constructor(private websocketEventsService: WebsocketEventsService) {

  }

  public connect(url): Rx.Subject<MessageEvent> {
    if (!this.subject) {
        this.subject = this.create(url);
    }
    return this.subject;
  }

   private create(url): Rx.Subject<MessageEvent> {

    let wse = new WebSocketEvent();
    wse.setState(0);
    wse.setDescription("Connecting...");
    this.websocketEventsService.sendMessage(wse);

    let ws = new WebSocket(url);

    let observable = Rx.Observable.create(
      (obs: Rx.Observer<MessageEvent>) => {
        ws.onmessage = obs.next.bind(obs); // important for messages handling
        //ws.onerror = obs.error.bind(obs);
        ws.onopen = (ev) => {
          this.onOpenHandler(ev);
        };
        ws.onclose = (ev) => {
          if (ev.wasClean) {
            this.onCloseHandler(ev);
          } else {
            this.onLostHandler(ev);
          }
        };

        return ws.close.bind(ws);
      })
    let observer = {
        next: (data: Object) => {
          if (ws.readyState === WebSocket.OPEN) {
            ws.send(JSON.stringify(data));
          }
        }
      }
     return Rx.Subject.create(observer, observable);
  }
  onOpenHandler(event: Event) {
    let wse = new WebSocketEvent();
    wse.setState(1);
    wse.setDescription("Connection established.");
    this.websocketEventsService.sendMessage(wse);
  }
  onCloseHandler(event: Event) {
    let wse = new WebSocketEvent();
    wse.setState(3);
    wse.setDescription("Connection closed clean.");
    this.websocketEventsService.sendMessage(wse);
  }
  onLostHandler(event: Event) {
    let wse = new WebSocketEvent();
    wse.setState(4);
    wse.setDescription("Connection lost.");
    this.websocketEventsService.sendMessage(wse);
  }
}
