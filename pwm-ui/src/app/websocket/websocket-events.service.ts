import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { WebSocketEvent } from './websocket-event'


@Injectable()
export class WebsocketEventsService {
  private subject = new Subject<any>();

  sendMessage(webSocketEvent: WebSocketEvent) {
      this.subject.next(webSocketEvent);
  }

  getMessage(): Observable<any> {
      return this.subject.asObservable();
  }
}
