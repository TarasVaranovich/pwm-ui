import { Component, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ControlDataService } from './control/control.service';
import { RestService } from './rest.service';
import { DataService } from './data/data.service';
import { DataPart } from './data/dataPart';
import { WebsocketEventsService } from './websocket/websocket-events.service';
import { WebSocketEvent} from './websocket/websocket-event';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  template: `
    <h1 class="pwm__title">{{title}}</h1>
    <button (click)="menuBtn()" id="pwmMenuMainBtn" class="pwm__menu_main_btn">menu</button>
    <div class="pwm__container">
      <nav id="navigation" class="pwm__menu">
          <a id="controlLink" routerLink="/control" routerLinkControl="active" class="pwm__menu_link pwm__menu__common_controls">control</a>
          <a routerLink="/data" routerLinkData="active" class="pwm__menu_link pwm__menu__common_controls">data</a>
          <a id="diagramLink" routerLink="/diagram" routerLinkDiagram="active" class="pwm__menu_link-disabled pwm__menu__common_controls">diagram</a>
          <button (click)="startBtn()" class="pwm__menu_btn pwm__menu__common_controls">start</button>
          <button (click)="stopBtn()" class="pwm__menu_btn pwm__menu__common_controls">stop</button>
          <button (click)="restartBtn()" class="pwm__menu_btn pwm__menu__common_controls">restart timeline</button>
          <button (click)="exitBtn()" class="pwm__menu_btn pwm__menu__common_controls">exit</button>
      </nav>
      <div class="pwm__outlet">
        <router-outlet></router-outlet>
      </div>
    </div>
    <div *ngIf="applicationAlertMessage" class="app__alert">
      <p class="app__alert_content">{{ applicationAlertMessage }}</p>
      <button (click)="applicationAlertBtn()" class="app__alert_btn">OK</button>
    </div>
    <div *ngIf="responceAlertMessage" class="app__alert">
      <p class="app__alert_content">{{ responceAlertMessage }}</p>
    </div>
    <div *ngIf="waitAlertMessage" class="app__alert">
      <p class="app__alert_content">{{ waitAlertMessage }}</p>
    </div>
  `,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'simple PWM modeling';
  subscription: Subscription;
  subscriptionSock: Subscription;
  applicationAlertMessage: string;
  responceAlertMessage: string;
  currentRouteUrl: string;
  waitAlertMessage: string;

  constructor(private communicationService: RestService,
              private controlDataService: ControlDataService,
              private dataService: DataService,
              private websocketEventsService: WebsocketEventsService,
              private router:Router) {

              this.applicationAlertMessage = null;
              this.responceAlertMessage = null;

              this.subscription = this.dataService.getMessage().subscribe(dataPart => {

                if(dataPart instanceof DataPart) {
                  let diagramLink = document.getElementById("diagramLink");
                  diagramLink.classList.remove("pwm__menu_link-disabled");
                  diagramLink.classList.add("pwm__menu_link");
                  this.waitAlertMessage = null;
                }

              });

              this.subscriptionSock = this.websocketEventsService.getMessage().subscribe(websocketevent => {

                if(websocketevent instanceof WebSocketEvent){
                  switch(websocketevent.getState()) {
                    case 0: {
                      this.responceAlertMessage = websocketevent.getDescription();
                      break;
                    }
                    case 1: {
                      this.responceAlertMessage = websocketevent.getDescription();
                      //close
                      setTimeout(function() {
                           this.responceAlertMessage = null;
                       }.bind(this), 1000);
                      break;
                    }
                    case 3: {
                      this.responceAlertMessage = null;
                      this.applicationAlertMessage = websocketevent.getDescription();
                      break;
                    }
                    case 4: {
                      this.responceAlertMessage = null;
                      this.applicationAlertMessage = websocketevent.getDescription();
                      break;
                    }
                  }
                }
              });
              //test
              this.router.navigate(['/introduction']);
              //end test
              router.events.subscribe(event => {

                  if (event instanceof NavigationEnd ) {
                    //console.log("current url",event.url);
                    this.currentRouteUrl = event.url;
                    this.closeMenu();
                  }

              });

    }
  exitBtn(): void{
    this.responceAlertMessage = "Request performing...";
    this.communicationService.exitProc().subscribe(
      restartData => {
        //this is successful responce
        this.responceAlertMessage = null;
      },
      err => {
        this.responceAlertMessage = null;
        this.applicationAlertMessage = 'Error! Something went wrong.';
      });
  }
  startBtn():void{
    this.closeMenu();
    this.responceAlertMessage = "Request performing...";
    this.communicationService.startProc(this.controlDataService.getMaxVoltage(),
                                        this.controlDataService.getDirectVoltage(),
                                        this.controlDataService.getVoltageDeflection(),
                                        this.controlDataService.getCalculationPrecision(),
                                        this.controlDataService.getPolesPairCount(),
                                        this.controlDataService.getNominalFrequence(),
                                        this.controlDataService.getRevolutions(),
                                        this.controlDataService.getRevolutionsChange(),
                                        this.controlDataService.getDeflectionChange()).subscribe(
      startData => {
        //this is successful responce
        let controlLink = document.getElementById("controlLink");
        controlLink.classList.add("pwm__menu_link-disabled");
        this.responceAlertMessage = null;
        if(this.currentRouteUrl == '/data' || this.currentRouteUrl == '/diagram') {
          this.waitAlertMessage = 'Waiting a data...';
        }
      },
      err => {
        this.responceAlertMessage = null;
        this.applicationAlertMessage = 'Error! Something went wrong.';
      }
    );
  }
  stopBtn():void{
    this.closeMenu();
    this.responceAlertMessage = "Request performing...";
    let diagramLink = document.getElementById("diagramLink");
    diagramLink.classList.remove("pwm__menu_link");
    diagramLink.classList.add("pwm__menu_link-disabled");

    this.communicationService.stopProc().subscribe(
      stopData => {
        //this is successful responce
        this.responceAlertMessage = null;
        let controlLink = document.getElementById("controlLink");
        controlLink.classList.remove("pwm__menu_link-disabled");
      },
      err => {
        this.responceAlertMessage = null;
        this.applicationAlertMessage = 'Error! Something went wrong.';
      }
    );
  }
  restartBtn():void{
    this.closeMenu();
    this.responceAlertMessage = "Request performing...";
    this.communicationService.restartProc().subscribe(
      restartData => {
        //this is successful responce
        this.responceAlertMessage = null;
      },
      err => {
        this.responceAlertMessage = null;
        this.applicationAlertMessage = 'Error! Something went wrong.';
      });
  }
  applicationAlertBtn(){
    this.applicationAlertMessage = null;
  }
  menuBtn() {
    let navigation = document.getElementById("navigation");
    let pwmMenuMainBtn = document.getElementById("pwmMenuMainBtn");
    if(navigation.classList.contains("pwm__menu-short")) {
      navigation.classList.remove("pwm__menu-short");
      pwmMenuMainBtn.innerHTML = 'menu';
    } else {
      navigation.classList.add("pwm__menu-short");
      pwmMenuMainBtn.innerHTML = 'close';
    }
  }

  private closeMenu() {
    let navigation = document.getElementById("navigation");
    let pwmMenuMainBtn = document.getElementById("pwmMenuMainBtn");
    navigation.classList.remove("pwm__menu-short");
    pwmMenuMainBtn.innerHTML = 'menu';
  }

}
