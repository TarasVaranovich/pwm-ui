import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { DataPart} from './dataPart';


@Injectable()
export class DataService {
  private subject = new Subject<any>();

  sendMessage(dataPart: DataPart) {
      this.subject.next(dataPart);
  }

  getMessage(): Observable<any> {
      return this.subject.asObservable();
  }
}
