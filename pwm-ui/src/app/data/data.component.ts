import 'rxjs/add/operator/toPromise';

import { Component, OnInit } from '@angular/core';
import { WebsocketService } from '../websocket/websocket.service';
import { ExchangeService } from '../websocket/exchange.service';
import { VectorRotationData } from './vectorRotationData';
import { DataService } from './data.service';
import { DataPart } from './dataPart';

@Component({
  selector: 'pwm-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {

  private counter: number;
  private maxMoment: number;
  private frequence: number;
  private revolutions: number;
  private vectorRotations: VectorRotationData[] = [];
  //private dataPart: DataPart;

  constructor(private exchangeService: ExchangeService,
              private dataService: DataService) {
    exchangeService.messages.subscribe(msg => {
      console.log('Response from websocket: ' + msg);
      //this.sockData = msg.maxMoment;
      /*json*/
      //console.log('Max moment:' + msg.maxMoment);
      //console.log('Frequence:' + msg.frequence);
      this.vectorRotations.length = 0; /*REFILL ARRAY*/
      this.counter = Number(msg.counter);
      this.maxMoment = Number(msg.maxMoment);
      this.frequence = Number(msg.frequence);
      this.revolutions = Number(msg.revolutions);
      let obj = JSON.parse(msg.tableData);
      //console.log(obj.toString);
      for(var i = 0; i < obj.length; i++) {
            let vrd = new VectorRotationData();
            vrd.id = Number(obj[i].id);
            vrd.u1 = Number(obj[i].U1);
            vrd.ua = Number(obj[i].Ua);
            vrd.ub = Number(obj[i].Ub);
            vrd.uc = Number(obj[i].Uc);
            vrd.t1 = Number(obj[i].t1);
            vrd.t2 = Number(obj[i].t2);
            vrd.t3 = Number(obj[i].t3);
            this.vectorRotations.push(vrd);
          }
          let dataPart = new DataPart();
          dataPart.setCounter(this.counter);
          dataPart.setFrequence(this.frequence);
          dataPart.setMaxMoment(this.maxMoment);
          dataPart.setRevolutions(this.revolutions);
          dataPart.setVectorRotations(this.vectorRotations);
          this.dataService.sendMessage(dataPart);
      //}
      /*end json*/
    });
  }

  ngOnInit() {

  }

}
