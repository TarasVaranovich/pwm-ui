import { VectorRotationData } from './vectorRotationData';

export class DataPart {

  private counter: number;
  private maxMoment: number;
  private frequence: number;
  private revolutions: number;
  private vectorRotations: VectorRotationData[] = [];

  public DataPart() {

  }

  public setCounter(counter: number): void {

    this.counter = counter;

  }

  public setMaxMoment(maxMoment: number): void {

    this.maxMoment = maxMoment;

  }

  public setFrequence(frequence: number): void {

    this.frequence = frequence;

  }

  public setRevolutions(revolutions: number): void {

    this.revolutions = revolutions;

  }

  public setVectorRotations(vectorRotations: VectorRotationData[]): void {

    this.vectorRotations = vectorRotations;

  }

  public getCounter(): number {

    return this.counter;

  }

  public getMaxMoment(): number {

    return this.maxMoment;

  }

  public getFrequence(): number {

    return this.frequence;

  }

  public getRevolutions(): number {

    return this.revolutions;

  }

  public getVectorRotations(): VectorRotationData[]{

    return this.vectorRotations;

  }

}
