export class VectorRotationData {
  id: number;
  u1: number;
  ua: number;
  ub: number;
  uc: number;
  t1: number;
  t2: number;
  t3: number;
}
