import { ElementRef } from '@angular/core';

export class DrawMechanicalDiagram{

  protected elementRef: ElementRef;
  protected ctx: CanvasRenderingContext2D;
  protected secondsCounter: number = 0;
  protected verticalValueSize: number = 345;
  protected verticalValuePecision: number;
  protected horizontalValuePecision: number = 10;
  protected timePeriodNumber: number = 0;
  //customisation:
  protected xSpace: number;
  protected ySpace: number = 30;
  protected yTextSpace: number = 20;
  private values: number[] = [];

  constructor(elementRef: ElementRef,
              ctx: CanvasRenderingContext2D,
              verticalValueSize: number,
              verticalValuePecision: number) {

               this.elementRef = elementRef;
               this.ctx = ctx;
               this.verticalValueSize = verticalValueSize;
               this.verticalValuePecision = verticalValuePecision;
               switch(true) {
                 case ((0 <= verticalValueSize) && (verticalValueSize < 100)): {
                   this.xSpace = 35;
                   break;
                 }
                 case ((100 <= verticalValueSize) && (verticalValueSize < 1000)): {
                   this.xSpace = 50;
                   break;
                 }
                 case ((1000 <= verticalValueSize) && (verticalValueSize < 10000)): {
                   this.xSpace = 70;
                   break;
                 }
                 default:{
                   this.xSpace = 50;
                   break;
                 }
               }
  }

  drawGrid(currentSecond:number) {

    if(this.secondsCounter > this.horizontalValuePecision) {
      this.timePeriodNumber +=1;
    }
    //DRAW GRID
    //->MAIN AXISES
    this.ctx.beginPath();
    this.ctx.lineWidth = 1.0;
    this.ctx.moveTo(this.xSpace, this.elementRef.nativeElement.height- this.ySpace);
    this.ctx.lineTo(this.xSpace, this.ySpace);
    this.ctx.moveTo(this.xSpace, this.elementRef.nativeElement.height- this.ySpace);
    this.ctx.lineTo(this.elementRef.nativeElement.width - this.xSpace, this.elementRef.nativeElement.height- this.ySpace);
    this.ctx.stroke();
    this.ctx.closePath();
    //->END MAIN AXISES
    //->VERTICAL MARKUP
    this.ctx.beginPath();
    this.ctx.moveTo(this.xSpace, this.elementRef.nativeElement.height - this.ySpace);
    this.ctx.lineWidth = 0.5;
    var verticalStep = (this.elementRef.nativeElement.height- this.ySpace * 2)/this.verticalValuePecision;
    var verticalMoveVal = verticalStep;
    var varticalValueStep = this.verticalValueSize/this.verticalValuePecision;
    var varticalValueCurrent = varticalValueStep;
    for(var i = 0; i < this.verticalValuePecision; i++) {
      this.ctx.moveTo(this.xSpace, this.elementRef.nativeElement.height- verticalMoveVal - this.ySpace);
      this.ctx.lineTo(this.elementRef.nativeElement.width - this.xSpace, this.elementRef.nativeElement.height- verticalMoveVal - this.ySpace);
      this.ctx.font = "20px Arial";
      this.ctx.textAlign = "right";
      this.ctx.fillText(String(varticalValueCurrent),this.xSpace * 0.8, this.elementRef.nativeElement.height- verticalMoveVal - this.ySpace + this.yTextSpace/2);
      verticalMoveVal = verticalMoveVal + verticalStep;
      varticalValueCurrent = varticalValueCurrent + varticalValueStep;
    }
    this.ctx.stroke();
    this.ctx.closePath();
    //->END VERTICAL MARKUP
    //NULL POINTER
    this.ctx.beginPath();
    this.ctx.font = "20px Arial";
    this.ctx.textAlign = "center"; //this.timePeriodNumber
    this.ctx.fillText(String(currentSecond),this.xSpace, this.elementRef.nativeElement.height - this.ySpace + this.yTextSpace);
    this.ctx.closePath();
    //END NULL POINTER
    //HORIZONTAL MARKUP
    this.ctx.beginPath();
    this.ctx.moveTo(this.xSpace, this.elementRef.nativeElement.height - this.ySpace);
    this.ctx.lineWidth = 0.5;
    var horizontalStep = (this.elementRef.nativeElement.width- this.xSpace * 2)/this.horizontalValuePecision;
    var horizontalMoveVal = horizontalStep;
    //var timePeriodDisplay = this.timePeriodNumber;
    var timePeriodDisplay = currentSecond + 1;
    for(var i = 0; i < this.horizontalValuePecision; i++) {
      this.ctx.moveTo(this.xSpace + horizontalMoveVal, this.elementRef.nativeElement.height - this.ySpace);
      this.ctx.lineTo(this.xSpace + horizontalMoveVal, this.ySpace);
      horizontalMoveVal = horizontalMoveVal + horizontalStep;
      this.ctx.font = "20px Arial";
      this.ctx.textAlign = "center";
      timePeriodDisplay = timePeriodDisplay + 1;
      this.ctx.fillText(String(timePeriodDisplay),this.xSpace + horizontalMoveVal - horizontalStep, this.elementRef.nativeElement.height - this.ySpace + this.yTextSpace);
    }
    this.ctx.stroke();
    this.ctx.closePath();
    //END HORIZONTAL MARKUP
    //END DRAW GRID
    this.secondsCounter +=1;

  }
  drawPoints(currentValue: number) {
        //values queue
       if(this.values.length < 11) {
         this.values.push(currentValue);
       } else {
         this.values.shift();
         this.values.push(currentValue);
       }
       //end values queue
       var horizontalMove = (this.elementRef.nativeElement.width- this.xSpace * 2)/this.horizontalValuePecision;
       this.ctx.fillStyle="blue";
       for(var timeNumber = 0; timeNumber < this.values.length; timeNumber++){
         var verticalMove = this.values[timeNumber] * (this.elementRef.nativeElement.height - 2 * this.ySpace)/this.verticalValueSize;
         this.ctx.beginPath();
         this.ctx.arc(horizontalMove * timeNumber + this.xSpace, this.elementRef.nativeElement.height - this.ySpace - verticalMove, 4, 0, 2 * Math.PI, true);
         this.ctx.fill();
         this.ctx.closePath();
       }
       this.ctx.fillStyle="black";
  }
  clearContext() {
    this.ctx.clearRect(0,0,this.elementRef.nativeElement.width, this.elementRef.nativeElement.height);
  }
}
