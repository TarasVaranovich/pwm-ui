import 'rxjs/add/operator/toPromise';

import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { DrawMechanicalDiagram} from './DrawMechanicalDiagram';
import { DataService } from '../../data/data.service';
import { DataPart } from '../../data/dataPart';
import { ControlDataService } from '../../control/control.service';

@Component({
  selector: 'diagram-mechanical',
  templateUrl: './diagram-mechanical.component.html',
  styleUrls: ['./diagram-mechanical.component.css']
})
export class DiagramMechanicalComponent implements OnInit {
  //visual discriptors
  @ViewChild('torqueDiagram') torqueRef: ElementRef;//#revolutionsDiagram
  @ViewChild('revolutionsDiagram') revolutionsRef: ElementRef;
  private ctxT: CanvasRenderingContext2D;
  private ctxR: CanvasRenderingContext2D;
  private drawMoment: DrawMechanicalDiagram;
  private drawRevolutions: DrawMechanicalDiagram;
  //end visual discriptors
  //getting data from socket
  private maxMoment: number;
  private revolutions: number;
  message: any;
  subscription: Subscription;
  //end getting data from socket

  constructor(private dataService: DataService,
              private controlDataService: ControlDataService) {
    this.subscription = this.dataService.getMessage().subscribe(dataPart => {

    if(dataPart instanceof DataPart) {
      this.drawTorqueDiagram(dataPart.getMaxMoment(), dataPart.getCounter());
      this.drawRevolutionsDiagram(dataPart.getRevolutions(), dataPart.getCounter());
    } else {
      console.log("DATA_TYPE_ERROR");
    }
    });
  }

  ngOnInit() {
    /*-----*/
    this.ctxR = this.revolutionsRef.nativeElement.getContext('2d');
    this.drawRevolutions = new DrawMechanicalDiagram(this.revolutionsRef,this.ctxR, this.controlDataService.getRevolutions(), 10);

    this.ctxT = this.torqueRef.nativeElement.getContext('2d');
    this.drawMoment = new DrawMechanicalDiagram(this.torqueRef,this.ctxT, this.controlDataService.getMaxTorque(), 5);
    /*-----*/
  }

  drawTorqueDiagram(torqueVal: number, currentSecond: number){
    this.drawMoment.clearContext();
    this.drawMoment.drawGrid(currentSecond);
    this.drawMoment.drawPoints(torqueVal);
  }

  drawRevolutionsDiagram(revolutionsVal: number, currentSecond: number){
    this.drawRevolutions.clearContext();
    this.drawRevolutions.drawGrid(currentSecond);
    this.drawRevolutions.drawPoints(revolutionsVal);
  }
}
