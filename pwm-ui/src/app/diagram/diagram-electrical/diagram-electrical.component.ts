import 'rxjs/add/operator/toPromise';

import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { DrawTimesDiagram } from './DrawTimesDiagram';
import { DrawVoltageDiagram } from './DrawVoltageDiagram';
import { DataService } from '../../data/data.service';
import { DataPart } from '../../data/dataPart';
import { ControlDataService } from '../../control/control.service';
import { VectorRotationData } from '../../data/vectorRotationData';
//for test
//import { VoltageMock} from './VoltageMock';

@Component({
  selector: 'diagram-mechanical',
  templateUrl: './diagram-electrical.component.html',
  styleUrls: ['./diagram-electrical.component.css']
})
export class DiagramElectricalComponent implements OnInit {
  //visual discriptors
  @ViewChild('voltageDiagram') voltageRef: ElementRef;
  @ViewChild('frequenceDiagram') frequenceRef: ElementRef;
  private ctxV: CanvasRenderingContext2D;
  private ctxT: CanvasRenderingContext2D;
  private ctxF: CanvasRenderingContext2D;
  private drawFrequence: DrawTimesDiagram;
  private drawVoltage: DrawVoltageDiagram;
  //end visual discriptors
  //getting data from socket
  private maxFrequence: number;
  private maxVoltage: number;
  message: any;
  subscription: Subscription;
  //end getting data from socket

  constructor(private dataService: DataService,
              private controlDataService: ControlDataService) {
    this.subscription = this.dataService.getMessage().subscribe(dataPart => {

    if(dataPart instanceof DataPart) {
        this.drawFrequenceDiagram(dataPart.getFrequence(),
                                  dataPart.getCounter(),
                                  dataPart.getVectorRotations());
        this.drawVoltageDiagram(dataPart.getVectorRotations());
        //this.drawTimesDiagram(dataPart.getVectorRotations());
    } else {
      console.log("DATA_TYPE_ERROR");
    }
    });
  }

  ngOnInit() {

    this.ctxV = this.voltageRef.nativeElement.getContext('2d');
    this.drawVoltage = new DrawVoltageDiagram(this.voltageRef,this.ctxV, this.controlDataService.getDirectVoltage(), 4, this.controlDataService.getCalculationPrecision());
    this.ctxF = this.frequenceRef.nativeElement.getContext('2d');
    this.drawFrequence = new DrawTimesDiagram(this.frequenceRef,this.ctxF, this.controlDataService.getMaxFrequence(), 10, this.controlDataService.getCalculationPrecision());

  }

  drawFrequenceDiagram(frequenceVal: number,
                       currentSecond: number,
                       vectorRotationData: VectorRotationData[]){ // button
    this.drawFrequence.clearContext();
    this.drawFrequence.drawGrid(currentSecond);
    this.drawFrequence.drawAngleGrid();
    this.drawFrequence.drawPoints(frequenceVal);
    this.drawFrequence.drawTimes(vectorRotationData);
  }

  drawVoltageDiagram(vectorRotationData: VectorRotationData[]){
    this.drawVoltage.clearContext();
    this.drawVoltage.drawGrid();
    this.drawVoltage.drawPoints(vectorRotationData);
  }

}
