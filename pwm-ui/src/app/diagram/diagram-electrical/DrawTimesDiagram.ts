import { ElementRef } from '@angular/core';
import { VectorRotationData } from '../../data/vectorRotationData';
import { DrawMechanicalDiagram} from '../diagram-mechanical/DrawMechanicalDiagram';

export class DrawTimesDiagram extends DrawMechanicalDiagram {
  private horizontalValuePecisionTimes: number = 10;
  constructor(elementRef: ElementRef,
              ctx: CanvasRenderingContext2D,
              verticalValueSize: number,
              verticalValuePecision: number,
              horizontalValuePecisionTimes: number) {
              super(elementRef, ctx, verticalValueSize, verticalValuePecision);
              this.horizontalValuePecisionTimes = horizontalValuePecisionTimes;
  }

  drawAngleGrid(){
    //HORIZONTAL MARKUP
    this.ctx.beginPath();
    this.ctx.strokeStyle = 'orange';
    this.ctx.moveTo(this.xSpace, this.elementRef.nativeElement.height - this.ySpace);
    this.ctx.lineWidth = 0.5;
    //console.log("Pres:" + this.horizontalValuePecisionTimes);
    var horizontalStep = this.horizontalValuePecisionTimes * (this.elementRef.nativeElement.width- this.xSpace * 2)/360;
    var horizontalMoveVal = 0;
    for(var i = 0; i <= 360; i = i + Number(this.horizontalValuePecisionTimes)) {
      horizontalMoveVal = horizontalMoveVal + horizontalStep;
      this.ctx.moveTo(this.xSpace + horizontalMoveVal - horizontalStep, this.elementRef.nativeElement.height - this.ySpace);
      this.ctx.lineTo(this.xSpace + horizontalMoveVal - horizontalStep, this.ySpace);
      this.ctx.font = "20px Arial";
      this.ctx.textAlign = "center";
      //console.log("i:" + i);
      if(i % 60 == 0){
        this.ctx.fillText(String(i),this.xSpace + horizontalMoveVal - horizontalStep, this.ySpace - this.yTextSpace/2);
      }
    }
    this.ctx.stroke();
    this.ctx.strokeStyle = 'black';
    this.ctx.closePath();

    //END HORIZONTAL MARKUP
  }

  drawTimes(periodData: VectorRotationData[]) {
    var verticalBase = this.elementRef.nativeElement.height - this.ySpace - (this.elementRef.nativeElement.height - 2 * this.ySpace)/3;
    var horizontalMove = this.xSpace;
    var horizontalStep = this.horizontalValuePecisionTimes * (this.elementRef.nativeElement.width- this.xSpace * 2)/360;
    var verticalT1Direct = verticalBase - (this.elementRef.nativeElement.height - 2 * this.ySpace)/8;
    var verticalT2Direct = verticalBase - 2 * (this.elementRef.nativeElement.height - 2 * this.ySpace)/8;
    var verticalT3Direct = verticalBase - 3 * (this.elementRef.nativeElement.height - 2 * this.ySpace)/8;
    for(var i = 0; i < periodData.length - 1; i++){
      var T1range = Math.abs(periodData[i].t1 * horizontalStep);
      var T2range = Math.abs(periodData[i].t2 * horizontalStep);
      var T3range = Math.abs(periodData[i].t3 * horizontalStep);
      this.drawT1(horizontalMove, verticalT3Direct, T1range);
      this.drawT2(horizontalMove, verticalT2Direct, T2range );
      this.drawT3(horizontalMove, verticalT1Direct, T3range);
      horizontalMove = horizontalMove + horizontalStep;
    }
  }
  drawT1(x: number, y: number, length: number){
    this.ctx.beginPath();
    this.ctx.moveTo(x, y);
    this.ctx.lineTo(x + length, y);
    this.ctx.lineWidth = 3;
    this.ctx.strokeStyle = 'yellow';
    this.ctx.stroke();
    this.ctx.lineWidth = 0.5;
    this.ctx.strokeStyle = 'black';
    this.ctx.closePath();

  }
  drawT2(x: number, y: number, length: number){
    this.ctx.beginPath();
    this.ctx.moveTo(x, y);
    this.ctx.lineTo(x + length, y);
    this.ctx.lineWidth = 3;
    this.ctx.strokeStyle = 'green';
    this.ctx.stroke();
    this.ctx.lineWidth = 0.5;
    this.ctx.strokeStyle = 'black';
    this.ctx.closePath();
  }
  drawT3(x: number, y: number, length: number){
    this.ctx.beginPath();
    this.ctx.moveTo(x, y);
    this.ctx.lineTo(x + length, y);
    this.ctx.lineWidth = 3;
    this.ctx.strokeStyle = 'red';
    this.ctx.stroke();
    this.ctx.lineWidth = 0.5;
    this.ctx.strokeStyle = 'black';
    this.ctx.closePath();
  }
}
