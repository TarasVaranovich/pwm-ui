import { ElementRef } from '@angular/core';
import { VectorRotationData } from '../../data/vectorRotationData';

export class DrawVoltageDiagram{
  private elementRef: ElementRef;
  private ctx: CanvasRenderingContext2D;
  private secondsCounter: number = 0;
  private verticalValueSize: number = 345;
  private verticalValuePecision: number;
  private horizontalValuePecision: number = 10;
  private timePeriodNumber: number = 0;
  //customisation:
  private xSpace: number;
  private ySpace: number = 30;
  private yTextSpace: number = 20;

  constructor(elementRef: ElementRef,
              ctx: CanvasRenderingContext2D,
              verticalValueSize: number,
              verticalValuePecision: number,
              horizontalValuePecision: number) {

               this.elementRef = elementRef;
               this.ctx = ctx;
               this.verticalValueSize = verticalValueSize;
               this.verticalValuePecision = verticalValuePecision;
               this.horizontalValuePecision = horizontalValuePecision;
               switch(true) {
                 case ((0 <= verticalValueSize) && (verticalValueSize < 100)): {
                   this.xSpace = 35;
                   break;
                 }
                 case ((100 <= verticalValueSize) && (verticalValueSize < 1000)): {
                   this.xSpace = 50;
                   break;
                 }
                 case ((1000 <= verticalValueSize) && (verticalValueSize < 10000)): {
                   this.xSpace = 70;
                   break;
                 }
                 default:{
                   this.xSpace = 50;
                   break;
                 }
               }
  }

  drawGrid() {

    //DRAW GRID
    //->MAIN AXISES
    this.ctx.beginPath();
    this.ctx.lineWidth = 1.0;
    this.ctx.moveTo(this.xSpace, this.elementRef.nativeElement.height- this.ySpace);
    this.ctx.lineTo(this.xSpace, this.ySpace);
    var horizontalAxis = this.ySpace + (this.elementRef.nativeElement.height - 2*this.ySpace)/2;
    this.ctx.moveTo(this.xSpace, horizontalAxis);
    this.ctx.lineTo(this.elementRef.nativeElement.width - this.xSpace, horizontalAxis);
    this.ctx.stroke();
    this.ctx.closePath();
    //->END MAIN AXISES
    //->VERTICAL MARKUP
    this.ctx.beginPath();
    this.ctx.moveTo(this.xSpace, this.elementRef.nativeElement.height - this.ySpace);
    this.ctx.lineWidth = 0.5;
    var verticalValueCount = this.verticalValuePecision * 2;
    var verticalStep = (this.elementRef.nativeElement.height- this.ySpace * 2)/this.verticalValuePecision;
    var verticalMoveVal = 0;
    var varticalValueStep = this.verticalValueSize/this.verticalValuePecision;
    var varticalValueCurrent = -this.verticalValueSize;
    //console.log(this.verticalValuePecision);
    for(var i = 0; i <= verticalValueCount; i++) {
      this.ctx.moveTo(this.xSpace, this.elementRef.nativeElement.height- verticalMoveVal - this.ySpace);
      this.ctx.lineTo(this.elementRef.nativeElement.width - this.xSpace, this.elementRef.nativeElement.height- verticalMoveVal - this.ySpace);
      this.ctx.font = "20px Arial";
      this.ctx.textAlign = "right";
      this.ctx.fillText(String(varticalValueCurrent),this.xSpace * 0.8, this.elementRef.nativeElement.height- verticalMoveVal - this.ySpace + this.yTextSpace/2);
      verticalMoveVal = verticalMoveVal + verticalStep;
      varticalValueCurrent = varticalValueCurrent + 2 * varticalValueStep;
    }
    this.ctx.stroke();
    this.ctx.closePath();
    //->END VERTICAL MARKUP
    //NULL POINTER
    this.ctx.beginPath();
    this.ctx.font = "20px Arial";
    this.ctx.textAlign = "center"; //this.timePeriodNumber
    this.ctx.fillText(String(0),this.xSpace, this.elementRef.nativeElement.height - this.ySpace + this.yTextSpace);
    this.ctx.closePath();
    //END NULL POINTER
    //HORIZONTAL MARKUP
    this.ctx.beginPath();
    this.ctx.moveTo(this.xSpace, this.elementRef.nativeElement.height - this.ySpace);
    this.ctx.lineWidth = 0.5;
    var horizontalStep = this.horizontalValuePecision * (this.elementRef.nativeElement.width- this.xSpace * 2)/360;
    var horizontalMoveVal = 0;
    for(var i = 0; i <= 360; i = i + Number(this.horizontalValuePecision)) {
      this.ctx.moveTo(this.xSpace + horizontalMoveVal, this.elementRef.nativeElement.height - this.ySpace);
      this.ctx.lineTo(this.xSpace + horizontalMoveVal, this.ySpace);
      horizontalMoveVal = horizontalMoveVal + horizontalStep;
      this.ctx.font = "20px Arial";
      this.ctx.textAlign = "center";
      if(i % 60 == 0){
        this.ctx.fillText(String(i),this.xSpace + horizontalMoveVal - horizontalStep, this.elementRef.nativeElement.height - this.ySpace + this.yTextSpace);
      }
    }
    this.ctx.stroke();
    this.ctx.closePath();
    //END HORIZONTAL MARKUP
    //END DRAW GRID
    this.secondsCounter +=1;

  }
  drawHorizontalLine() {

  }
  drawPoints(periodData: VectorRotationData[]) {
       var verticalBase = this.elementRef.nativeElement.height - this.ySpace - (this.elementRef.nativeElement.height - 2 * this.ySpace)/2;
       //var verticalPosition = verticalBase - (-285/this.verticalValueSize) * (this.elementRef.nativeElement.height - 2 * this.ySpace)/2;
       var horizontalMove = this.xSpace;
       var horizontalStep = this.horizontalValuePecision * (this.elementRef.nativeElement.width- this.xSpace * 2)/360;
       for(var i = 0; i < periodData.length; i++){
         var verticalU1 = verticalBase - (periodData[i].u1/this.verticalValueSize) * (this.elementRef.nativeElement.height - 2 * this.ySpace)/2;
         this.drawU1Voltage(horizontalMove, verticalU1);
         var verticalUa = verticalBase - (periodData[i].ua/this.verticalValueSize) * (this.elementRef.nativeElement.height - 2 * this.ySpace)/2;
         this.drawUaVoltage(horizontalMove, verticalUa);
         var verticalUb = verticalBase - (periodData[i].ub/this.verticalValueSize) * (this.elementRef.nativeElement.height - 2 * this.ySpace)/2;
         this.drawUbVoltage(horizontalMove, verticalUb);
         var verticalUc = verticalBase - (periodData[i].uc/this.verticalValueSize) * (this.elementRef.nativeElement.height - 2 * this.ySpace)/2;
         this.drawUcVoltage(horizontalMove, verticalUc);
         horizontalMove = horizontalMove + horizontalStep;
       }
  }
  drawU1Voltage(x: number, y: number){
    this.ctx.fillStyle="blue";
    this.ctx.beginPath();
    this.ctx.arc(x, y, 4, 0, 2 * Math.PI, true);
    this.ctx.fill();
    this.ctx.closePath();
    this.ctx.fillStyle="black";
  }
  drawUaVoltage(x: number, y: number){
    this.ctx.fillStyle="yellow";
    this.ctx.beginPath();
    this.ctx.arc(x, y, 4, 0, 2 * Math.PI, true);
    this.ctx.fill();
    this.ctx.closePath();
    this.ctx.fillStyle="black";
  }
  drawUbVoltage(x: number, y: number){
    this.ctx.fillStyle="green";
    this.ctx.beginPath();
    this.ctx.arc(x, y, 4, 0, 2 * Math.PI, true);
    this.ctx.fill();
    this.ctx.closePath();
    this.ctx.fillStyle="black";
  }
  drawUcVoltage(x: number, y: number){
    this.ctx.fillStyle="red";
    this.ctx.beginPath();
    this.ctx.arc(x, y, 4, 0, 2 * Math.PI, true);
    this.ctx.fill();
    this.ctx.closePath();
    this.ctx.fillStyle="black";
  }

  clearContext() {
    this.ctx.clearRect(0,0,this.elementRef.nativeElement.width, this.elementRef.nativeElement.height);
  }
}
