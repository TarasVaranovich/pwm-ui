import { VectorRotationData } from '../../data/vectorRotationData';

export class VoltageMock {

  private vectorRotations: VectorRotationData[] = [];

  constructor(){
    let vectorData = new VectorRotationData();
    vectorData.id = 0;
    vectorData.t1 = 0.3;
    vectorData.t2 = 0.3;
    vectorData.t3 = 0.4;
    vectorData.u1 = 130;
    vectorData.ua = 100;
    vectorData.ub = -100;
    vectorData.uc = -100;
    this.vectorRotations.push(vectorData);
    let vectorData2 = new VectorRotationData();
    vectorData2.id = 1;
    vectorData2.t1 = 0.2;
    vectorData2.t2 = 0.2;
    vectorData2.t3 = 0.6;
    vectorData2.u1 = -130;
    vectorData2.ua = -100;
    vectorData2.ub = 100;
    vectorData2.uc = 100;
    this.vectorRotations.push(vectorData2);
  }

  public getData():VectorRotationData[] {
    return this.vectorRotations;
  }
}
