import { pwm-ui } from './app.po';

describe('project30.1 App', () => {
  let page: pwm-ui;

  beforeEach(() => {
    page = new pwm-ui();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
