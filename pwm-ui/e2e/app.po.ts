import { browser, by, element } from 'protractor';

export class pwm-ui {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }
}
